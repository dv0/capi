import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { from, map, Observable } from 'rxjs';

import { UserRepository } from 'src/user/user.repository';

import { PaginationUserRequestDto } from 'src/user/models/dto/pagination-user-request.dto';
import { UserEntity } from 'src/user/models/entities/user.entity';
import { IUser } from 'src/user/models/user.interface';

@Injectable()
export class UserPaginationService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
  ) {}

  public paginateUsers(
    options: IPaginationOptions,
  ): Observable<Pagination<IUser>> {
    return from(paginate<UserEntity>(this.userRepository, options)).pipe(
      map((usersPageable: Pagination<UserEntity>) =>
        this.deletePaginationPassword(usersPageable),
      ),
    );
  }

  private deletePaginationPassword(
    usersPageable: Pagination<UserEntity>,
  ): Pagination<IUser> {
    usersPageable.items.forEach((user) => {
      delete user.password;
    });

    return usersPageable;
  }

  public paginateFilterByUsername(
    request: PaginationUserRequestDto,
  ): Observable<Pagination<IUser>> {
    return this.userRepository.paginateByUsername(request).pipe(
      map(([users, totalUsers]) => {
        return this.pageableUsers(users, totalUsers, request);
      }),
    );
  }

  private pageableUsers(
    users: UserEntity[],
    totalUsers: number,
    request: PaginationUserRequestDto,
  ): Pagination<IUser> {
    const totalPages = Math.ceil(totalUsers / request.limit);

    return {
      items: users,
      links: {
        first: request.route + `?limit=${request.limit}`,
        previous:
          request.page > 0 || request.page === null
            ? request.route +
              `?limit=${request.limit}&page=${Number(request.page) - 1}`
            : ``,
        next:
          totalPages === Number(request.page)
            ? ''
            : request.route +
              `?limit=${request.limit}&page=${Number(request.page) + 1}`,
        last: request.route + `?limit=${request.limit}&page=${totalPages}`,
      },
      meta: {
        currentPage: request.page,
        itemCount: users.length,
        itemsPerPage: request.limit,
        totalItems: totalUsers,
        totalPages: totalPages,
      },
    };
  }
}
