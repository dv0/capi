import { Test, TestingModule } from '@nestjs/testing';
import { UserPaginationService } from './user-pagination.service';

describe('UserPaginationService', () => {
  let service: UserPaginationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserPaginationService],
    }).compile();

    service = module.get<UserPaginationService>(UserPaginationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
