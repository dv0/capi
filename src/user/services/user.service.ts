import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { DeleteResult, UpdateResult } from 'typeorm';

import { UserRepository } from '../user.repository';

import { UserCredentialsDto } from '../models/dto/user-credentials.dto';
import { UserEntity } from '../models/entities/user.entity';
import { ILoggedInUser } from '../models/logged-in-user.interface';
import { IUser } from '../models/user.interface';

import { AuthService } from 'src/auth/services/auth/auth.service';

@Injectable()
export class UserService {
  constructor(
    private authService: AuthService,
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
  ) {}

  public create(user: UserEntity): Observable<IUser> {
    return this.authService.hashPassword(user.password).pipe(
      switchMap((passwordHash: string) =>
        this.userRepository.createUser(user, passwordHash).pipe(
          catchError((err: HttpException) => {
            throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
          }),
          map((savedUser: UserEntity) => {
            if (savedUser) {
              return this.getUserWithoutPassword(savedUser);
            }
          }),
        ),
      ),
    );
  }

  public login(authCredentials: UserCredentialsDto): Observable<ILoggedInUser> {
    return this.validateUser(authCredentials).pipe(
      switchMap((currentUser: UserEntity) => {
        return this.authService
          .generateJwt(currentUser)
          .pipe(map((jwt: string) => ({ ...currentUser, token: jwt })));
      }),
    );
  }

  public updateUser(id: number, user: UserEntity): Observable<IUser> {
    return this.userRepository.updateUser(id, user).pipe(
      map((user: UserEntity) => {
        if (user) {
          return this.getUserWithoutPassword(user);
        } else {
          throw new HttpException(
            `User doesn't exist.`,
            HttpStatus.BAD_REQUEST,
          );
        }
      }),
    );
  }

  public updateUserRole(
    id: number,
    user: UserEntity,
  ): Observable<UpdateResult> {
    return from(this.userRepository.update(id, user)).pipe(
      catchError((err: HttpException) => {
        throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
      }),
    );
  }

  public findUser(id: number): Observable<IUser> {
    return this.userRepository.findUserById(id);
  }

  public deleteUser(id: number): Observable<DeleteResult> {
    return from(this.userRepository.delete(id));
  }

  private validateUser(authCredentials: UserCredentialsDto): Observable<IUser> {
    return this.userRepository.findUserByEmail(authCredentials.email).pipe(
      switchMap((user: UserEntity) => {
        if (user) {
          return this.authService
            .comparePasswords(authCredentials.password, user.password)
            .pipe(
              map((match: boolean) => {
                if (match) {
                  return this.getUserWithoutPassword(user);
                } else {
                  throw new HttpException(
                    `Wrong password.`,
                    HttpStatus.BAD_REQUEST,
                  );
                }
              }),
            );
        } else {
          throw new HttpException(
            `User doesn't exist.`,
            HttpStatus.BAD_REQUEST,
          );
        }
      }),
    );
  }

  private getUserWithoutPassword(user: UserEntity): IUser {
    const { password, ...result } = user;

    return result;
  }
}
