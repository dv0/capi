import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from './controllers/user.controller';
import { UserPaginationService } from './services/user-pagination/user-pagination.service';
import { UserService } from './services/user.service';
import { UserRepository } from './user.repository';

import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([UserRepository]), AuthModule],
  providers: [UserService, UserPaginationService],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
