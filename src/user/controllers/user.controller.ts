import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Observable } from 'rxjs';
import { DeleteResult, UpdateResult } from 'typeorm';

import { UserPaginationService } from '../services/user-pagination/user-pagination.service';

import { ILoggedInUser } from '../models/logged-in-user.interface';
import { UserRole } from '../models/user-role.enum';

import { hasRoles } from 'src/auth/decorator/roles.decorator';
import { CurrentUserGuard } from 'src/auth/guards/current-user.guard';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { UserService } from 'src/user/services/user.service';

import { UserCredentialsDto } from 'src/user/models/dto/user-credentials.dto';
import { PaginationUserRequestDto } from 'src/user/models/dto/pagination-user-request.dto';
import { UserEntity } from 'src/user/models/entities/user.entity';
import { IUser } from 'src/user/models/user.interface';

@Controller('users')
export class UserController {
  constructor(
    private userService: UserService,
    private userPaginationService: UserPaginationService,
  ) {}

  @Post()
  @hasRoles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  createUser(@Body() user: UserEntity): Observable<UserEntity> | object {
    return this.userService.create(user);
  }

  @Post('login')
  login(
    @Body() authCredentials: UserCredentialsDto,
  ): Observable<ILoggedInUser | string> {
    return this.userService.login(authCredentials);
  }

  @Put(':id')
  @UseGuards(JwtAuthGuard, CurrentUserGuard)
  updateUser(
    @Param('id') id: string,
    @Body() user: UserEntity,
  ): Observable<IUser> {
    return this.userService.updateUser(Number(id), user);
  }

  @Put(':id/role')
  @hasRoles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  updateUserRole(
    @Param('id') id: string,
    @Body() user: UserEntity,
  ): Observable<UpdateResult> {
    return this.userService.updateUserRole(Number(id), user);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  findUser(@Param() params): Observable<IUser> {
    return this.userService.findUser(params.id);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, CurrentUserGuard)
  deleteUser(@Param('id') id: string): Observable<DeleteResult> {
    return this.userService.deleteUser(Number(id));
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe({ transform: true }))
  userIndex(
    @Query() request: PaginationUserRequestDto,
  ): Observable<Pagination<IUser>> {
    request.limit = request.limit > 100 ? 100 : request.limit;

    return request.username === null || request.username === undefined
      ? this.userPaginationService.paginateUsers(request)
      : this.userPaginationService.paginateFilterByUsername(request);
  }
}
