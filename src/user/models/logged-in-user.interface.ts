import { IUser } from './user.interface';

export interface ILoggedInUser extends IUser {
  token: string;
}
