import { UserRole } from './user-role.enum';

export interface IUser {
  id: number;

  name: string;

  username: string;

  email: string;

  role: UserRole;
}
