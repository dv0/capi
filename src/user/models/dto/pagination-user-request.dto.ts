import { IPaginationOptions } from 'nestjs-typeorm-paginate';

export class PaginationUserRequestDto implements IPaginationOptions {
  limit = 10;

  page = 1;

  route = `${process.env.URL}/users`;

  username: string;
}
