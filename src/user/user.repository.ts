import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EntityRepository, Repository, Like } from 'typeorm';

import { PaginationUserRequestDto } from './models/dto/pagination-user-request.dto';
import { UserEntity } from './models/entities/user.entity';
import { UserRole } from './models/user-role.enum';

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {
  public createUser(
    user: UserEntity,
    passwordHash: string,
  ): Observable<UserEntity> {
    const newUser = this.create({
      name: user.name,
      username: user.username,
      email: user.email,
      password: passwordHash,
      role: UserRole.USER,
    });

    return from(this.save(newUser));
  }

  public updateUser(id: number, user: UserEntity): Observable<UserEntity> {
    return from(this.update(id, user)).pipe(
      switchMap(() => this.findUserById(id)),
    );
  }

  public findUserById(id: number): Observable<UserEntity> {
    return from(this.findOne({ id }));
  }

  public findUserByEmail(email: string): Observable<UserEntity> {
    return from(
      this.findOne(
        { email },
        {
          select: ['id', 'password', 'name', 'username', 'email', 'role'],
        },
      ),
    );
  }

  public paginateByUsername(
    request: PaginationUserRequestDto,
  ): Observable<[UserEntity[], number]> {
    const page = Number(request.page) !== 0 ? Number(request.page) - 1 : 0;

    return from(
      this.findAndCount({
        skip: page * Number(request.limit) || 0,
        take: Number(request.limit) || 10,
        order: { id: 'ASC' },
        select: ['id', 'name', 'username', 'email', 'role'],
        where: [{ username: Like(`%${request.username}%`) }],
      }),
    );
  }
}
