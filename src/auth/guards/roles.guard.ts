import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserRepository } from 'src/user/user.repository';

import { UserEntity } from 'src/user/models/entities/user.entity';
import { IUser } from 'src/user/models/user.interface';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private reflector: Reflector,
  ) {}

  canActivate(context: ExecutionContext): Observable<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    if (!roles) {
      return of(true);
    }

    const request = context.switchToHttp().getRequest();
    const user: IUser = request.user;

    return from(this.userRepository.findOne(user.id)).pipe(
      map((foundUser: UserEntity) => {
        const hasRole = () => roles.indexOf(foundUser.role) > -1;
        let hasPermission = false;

        if (hasRole()) {
          hasPermission = true;
        }

        return foundUser && hasPermission;
      }),
    );
  }
}
