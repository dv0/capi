import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserRepository } from 'src/user/user.repository';

import { UserRole } from 'src/user/models/user-role.enum';
import { UserEntity } from 'src/user/models/entities/user.entity';
import { IUser } from 'src/user/models/user.interface';

@Injectable()
export class CurrentUserGuard implements CanActivate {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
  ) {}

  canActivate(context: ExecutionContext): Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const params = request.params;
    const user: IUser = request.user;

    return from(this.userRepository.findUserById(user.id)).pipe(
      map((currentUser: UserEntity) => {
        let hasPermission = false;

        if (
          currentUser.id === Number(params.id) ||
          currentUser.role === UserRole.ADMIN
        ) {
          hasPermission = true;
        }

        return currentUser && hasPermission;
      }),
    );
  }
}
