import { SetMetadata } from '@nestjs/common';

export const hasRoles = (...rolesCheck: string[]) =>
  SetMetadata('roles', rolesCheck);
