import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { from, Observable } from 'rxjs';
import bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  /**
   * Generate an auth token using JWT
   * @param user object representing platform user.
   */
  public generateJwt(user: any): Observable<string> {
    return from(this.jwtService.signAsync({ user }));
  }

  public hashPassword(password: string): Observable<string> {
    return from<Promise<string>>(bcrypt.hash(password, 12));
  }

  public comparePasswords(
    newPassword: string,
    passwordHash: string,
  ): Observable<boolean> {
    return from(bcrypt.compare(newPassword, passwordHash));
  }
}
